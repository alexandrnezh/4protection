﻿using System.Web;
using System.Web.Optimization;

namespace Forprotection
{
    public class BundleConfig
    {
        // Дополнительные сведения об объединении см. на странице https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
            // готово к выпуску, используйте средство сборки по адресу https://modernizr.com, чтобы выбрать только необходимые тесты.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/articles.css",
                      "~/Content/faq.css",
                      "~/Content/layout.css",
                      "~/Content/main.css",
                      "~/Content/owl.carousel.css",
                      "~/Content/owl.carousel.min.css",
                      "~/Content/owl.theme.default.min.css",
                      "~/Content/owl.theme.green.min.css",
                      "~/Content/order.css",
                      "~/Content/order_1.css",
                      "~/Content/edit.css",
                      "~/Content/edit_article.css",
                      "~/Content/login.css",
                      "~/Content/order_admin.css",
                      "~/Content/order_success.css",
                      "~/Content/product_comment.css"

                      ));
        }
    }
}
