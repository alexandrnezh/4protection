﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Forprotection.Startup))]
namespace Forprotection
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
