﻿using Forprotection.Models;
using Forprotection.Models.DB;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Forprotection.Controllers
{
    public class HomeController : Controller
    {
        List<ApplicationUser> users = new List<ApplicationUser>();
        List<Product> products = new List<Product>();
        List<ProductImage> productImages = new List<ProductImage>();
        List<AccountInfo> accountInfos = new List<AccountInfo>();
        List<Article> articles = new List<Article>();
        List<ArticleImage> articleImages = new List<ArticleImage>();
        List<Order> orders = new List<Order>();
        List<OrderItem> orderItems = new List<OrderItem>();
        List<Promocode> promocodes = new List<Promocode>();
        List<FAQ> faqs = new List<FAQ>();
        List<Comment> comments = new List<Comment>();
        List<SliderImage> sliderImages = new List<SliderImage>();

        //[RequireHttps]
        public ActionResult Index()
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                products = ctx.Products.ToList();
                productImages = ctx.ProductImages.ToList();
                sliderImages = ctx.SliderImages.ToList();
            }
            return View(new ProductSliderViewModel { Products = products, SliderImages = sliderImages });
        }

        [RequireHttps]
        public ActionResult Articles()
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                users = ctx.Users.ToList();
                articles = ctx.Articles.ToList();
                articleImages = ctx.ArticleImages.ToList();
            }
            return View(articles);
        }
        [RequireHttps]
        [Authorize(Roles = RoleConfig.ADMIN)]
        public ActionResult Promocodes()
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                promocodes = ctx.Promocodes.ToList();
            }
            return View(promocodes);
        }

        [RequireHttps]
        [Authorize(Roles = RoleConfig.ADMIN)]
        public ActionResult EditProduct(int? id)
        {
            if (id != null)
            {
                var product = new Product();
                using (ApplicationDbContext ctx = new ApplicationDbContext())
                {
                    product = ctx.Products.FirstOrDefault(p => p.Id == id);
                }
                return View(product);
            }
            else
            {
                return RedirectToAction("Index");
            }

        }
        [RequireHttps]
        public ActionResult ViewProductComment(int? id)
        {
            if (id != null)
            {
                var product = new Product();
                using (ApplicationDbContext ctx = new ApplicationDbContext())
                {
                    comments = ctx.Comments.Where(c => c.Product.Id == id).ToList();
                    productImages = ctx.ProductImages.ToList();
                    product = ctx.Products.FirstOrDefault(p => p.Id == id);
                }
                return View(new ProductViewModel { Product = product, Comments = comments });
            }
            else
            {
                return RedirectToAction("Index");
            }

        }
        [RequireHttps]
        [HttpPost]
        public ActionResult ViewProductComment(int product_id, string nickname, string text, DateTime date)
        {
            var product = new Product();
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                product = ctx.Products.FirstOrDefault(c => c.Id == product_id);
                ctx.Comments.Add(new Comment { Product = product, Nickname = nickname, Text = text, CreatedDate = date });
                ctx.SaveChanges();
            }
            return RedirectToAction("ViewProductComment", new { id = product_id });

        }
        [RequireHttps]
        [Authorize(Roles = RoleConfig.ADMIN)]
        [HttpPost]
        public ActionResult EditProduct(Product product, HttpPostedFileBase uploadFile)
        {
            if (product != null)
            {
                using (ApplicationDbContext ctx = new ApplicationDbContext())
                {
                    var images = ctx.ProductImages.ToList();
                    var Product = ctx.Products.FirstOrDefault(p => p.Id == product.Id);
                    Product.Name = product.Name;
                    Product.Price = product.Price;
                    Product.Description = product.Description;
                    Product.ExtraDescription = product.ExtraDescription;
                    Product.Nal = product.Nal;
                    if (uploadFile != null && uploadFile.ContentLength > 0)
                    {
                        Product.Image.Path = $"Images/{uploadFile.FileName}";
                        string filePath = Path.Combine(Server.MapPath("/Images"), Path.GetFileName(uploadFile.FileName));
                        uploadFile.SaveAs(filePath);
                    }
                    ctx.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }
        [RequireHttps]
        [Authorize(Roles = RoleConfig.ADMIN)]
        public ActionResult CreateProduct()
        {
            return View();
        }
        [RequireHttps]
        [Authorize(Roles = RoleConfig.ADMIN)]
        [HttpPost]
        public ActionResult CreateProduct(Product p, HttpPostedFileBase uploadFile)
        {
            if (p != null)
            {
                using (ApplicationDbContext ctx = new ApplicationDbContext())
                {
                    Product product = new Product { Name = p.Name, Price = p.Price, Description = p.Description, ExtraDescription = p.ExtraDescription, DateCreated = DateTime.Now, Nal = p.Nal };
                    if (uploadFile != null && uploadFile.ContentLength > 0)
                    {
                        ProductImage image = new ProductImage { Path = $"Images/{uploadFile.FileName}" };
                        product.Image = image;
                        string filePath = Path.Combine(Server.MapPath("/Images"), Path.GetFileName(uploadFile.FileName));
                        uploadFile.SaveAs(filePath);
                    }
                    ctx.Products.Add(product);
                    ctx.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }
        [RequireHttps]
        [Authorize(Roles = RoleConfig.ADMIN)]
        public ActionResult CreatePromocode()
        {
            return View();
        }

        [Authorize(Roles = RoleConfig.ADMIN)]
        [HttpPost]
        public ActionResult CreatePromocode(Promocode p)
        {
            if (p != null)
            {
                using (ApplicationDbContext ctx = new ApplicationDbContext())
                {
                    var user = ctx.Users.FirstOrDefault(u => u.UserName == User.Identity.Name);
                    Promocode promocode = new Promocode { Code = p.Code, DiscountPercent = p.DiscountPercent, User = user };
                    ctx.Promocodes.Add(promocode);
                    ctx.SaveChanges();
                }
                return RedirectToAction("Promocodes");
            }
            else
            {
                return RedirectToAction("Promocodes");
            }

        }
        [RequireHttps]
        [Authorize(Roles = RoleConfig.ADMIN)]
        public ActionResult DeletePromocode(int? id)
        {
            if (id != null)
            {
                using (ApplicationDbContext ctx = new ApplicationDbContext())
                {
                    ctx.Promocodes.Remove(ctx.Promocodes.FirstOrDefault(p => p.Id == id));
                    ctx.SaveChanges();
                }
                return RedirectToAction("Promocodes");
            }
            else
            {
                return RedirectToAction("Promocodes");
            }

        }
        [RequireHttps]
        [Authorize(Roles = RoleConfig.ADMIN)]
        public ActionResult DeleteProduct(int? id)
        {
            if (id != null)
            {
                using (ApplicationDbContext ctx = new ApplicationDbContext())
                {
                    ctx.Products.Remove(ctx.Products.FirstOrDefault(p => p.Id == id));
                    ctx.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }
        [RequireHttps]
        [Authorize(Roles = RoleConfig.ADMIN)]
        public ActionResult EditArticle(int? id)
        {
            if (id != null)
            {
                var article = new Article();
                using (ApplicationDbContext ctx = new ApplicationDbContext())
                {
                    article = ctx.Articles.FirstOrDefault(a => a.Id == id);
                }
                return View(article);
            }
            else
            {
                return RedirectToAction("Index");
            }

        }
        [RequireHttps]
        [Authorize(Roles = RoleConfig.ADMIN)]
        [HttpPost]
        public ActionResult EditArticle(Article article, HttpPostedFileBase uploadFile)
        {
            if (article != null)
            {
                using (ApplicationDbContext ctx = new ApplicationDbContext())
                {
                    var images = ctx.ArticleImages.ToList();
                    var Article = ctx.Articles.FirstOrDefault(a => a.Id == article.Id);

                    Article.Name = article.Name;
                    Article.Body = article.Body;
                    if (uploadFile != null && uploadFile.ContentLength > 0)
                    {
                        Article.Image.Path = $"Images/{uploadFile.FileName}";
                        string filePath = Path.Combine(Server.MapPath("/Images"), Path.GetFileName(uploadFile.FileName));
                        uploadFile.SaveAs(filePath);
                    }
                    ctx.SaveChanges();
                }
                return RedirectToAction("Articles");
            }
            else
            {
                return RedirectToAction("Articles");
            }

        }
        [RequireHttps]
        [Authorize(Roles = RoleConfig.ADMIN)]
        public ActionResult CreateArticle()
        {
            return View();
        }
        [RequireHttps]
        [Authorize(Roles = RoleConfig.ADMIN)]
        [HttpPost]
        public ActionResult CreateArticle(Article a, HttpPostedFileBase uploadFile)
        {
            if (a != null)
            {
                using (ApplicationDbContext ctx = new ApplicationDbContext())
                {
                    var user = ctx.Users.FirstOrDefault(u => u.Email == User.Identity.Name);
                    Article article = new Article { Body = a.Body, Name = a.Name, DateCreated = DateTime.Now, User = user };
                    if (uploadFile != null && uploadFile.ContentLength > 0)
                    {
                        ArticleImage image = new ArticleImage { Path = $"Images/{uploadFile.FileName}" };
                        article.Image = image;
                        string filePath = Path.Combine(Server.MapPath("/Images"), Path.GetFileName(uploadFile.FileName));
                        uploadFile.SaveAs(filePath);
                    }
                    ctx.Articles.Add(article);
                    ctx.SaveChanges();
                }
                return RedirectToAction("Articles");
            }
            else
            {
                return RedirectToAction("Articles");
            }

        }
        [RequireHttps]
        [Authorize(Roles = RoleConfig.ADMIN)]
        public ActionResult DeleteArticle(int? id)
        {
            if (id != null)
            {
                using (ApplicationDbContext ctx = new ApplicationDbContext())
                {
                    ctx.Articles.Remove(ctx.Articles.FirstOrDefault(a => a.Id == id));
                    ctx.SaveChanges();
                }
                return RedirectToAction("Articles");
            }
            else
            {
                return RedirectToAction("Articles");
            }

        }
        [RequireHttps]
        [HttpPost]
        public ActionResult Trash(int Id, int? quantity)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (quantity is null)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    int Quantity = (int)quantity;

                  
                    List<OrderItem> orderItems = new List<OrderItem>();
                    if (Session["Trash"] is null)
                    {

                    }
                    else
                    {
                        orderItems = Session["Trash"] as List<OrderItem>;
                    }
                    using (ApplicationDbContext ctx = new ApplicationDbContext())
                    {
                        productImages = ctx.ProductImages.ToList();
                        Product product = ctx.Products.First(p => p.Id == Id);

                        OrderItem orderItem = new OrderItem { Guid = Guid.NewGuid(), Quantity = Quantity, Price = Quantity * product.Price, Product = product };
                        ctx.OrderItems.Add(orderItem);
                        ctx.SaveChanges();
                        orderItems.Add(ctx.OrderItems.FirstOrDefault(oi => oi.Guid == orderItem.Guid));
                        List<OrderItem> os = ctx.OrderItems.ToList();
                        ;
                    }

                    Session["Trash"] = orderItems;
                    return RedirectToAction("Order");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }



        }
        [RequireHttps]
        [Authorize]
        public ActionResult Order()
        {

            List<OrderItem> orderItems = new List<OrderItem>();
            orderItems = Session["Trash"] as List<OrderItem>;

            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                productImages = ctx.ProductImages.ToList();
                products = ctx.Products.ToList();
            }
            return View(orderItems);

        }
        [RequireHttps]
        public ActionResult DeleteOrderItem(int? id)
        {
            if (id is null)
            {
                return RedirectToAction("Order");
            }
            else
            {
                List<OrderItem> orderItems = new List<OrderItem>();
                orderItems = Session["Trash"] as List<OrderItem>;
                using (ApplicationDbContext ctx = new ApplicationDbContext())
                {
                    ctx.OrderItems.Remove(ctx.OrderItems.FirstOrDefault(oi => oi.Id == id));
                    ctx.SaveChanges();
                }
                orderItems.Remove(orderItems.FirstOrDefault(oi => oi.Id == id));
                if (orderItems.Count <= 0)
                {
                    Session["Trash"] = null;
                }
                else
                {
                    Session["Trash"] = orderItems;
                }
                return RedirectToAction("Order");
            }

        }
        [RequireHttps]
        [HttpPost]
        public ActionResult Order(string Promocode, decimal Price)
        {
            Promocode promocode;
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {

                if (!ctx.Promocodes.Any(p => p.Code == Promocode))
                {
                    promocode = null;
                    return View("OrderCreate", new PromocodeViewModel { Promocode = promocode, Price = Price });
                }
                else
                {

                    promocode = ctx.Promocodes.FirstOrDefault(p => p.Code == Promocode);
                    Price = Price - ((Price / 100) * (promocode.DiscountPercent));
                    return View("OrderCreate", new PromocodeViewModel { Promocode = promocode, Price = Price });
                }

            }

        }
        [RequireHttps]
        [Authorize]
        public ActionResult OrderCreate(string Promocode, decimal Price)
        {
            Promocode promocode;
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {

                if (!ctx.Promocodes.Any(p => p.Code == Promocode))
                {
                    promocode = null;
                    return View(new PromocodeViewModel { Promocode = promocode, Price = Price });
                }
                else
                {

                    promocode = ctx.Promocodes.FirstOrDefault(p => p.Code == Promocode);
                    Price = Price - ((Price / 100) * (promocode.DiscountPercent));
                    return View(new PromocodeViewModel { Promocode = promocode, Price = Price });
                }

            }


        }
        [RequireHttps]
        [Authorize]
        [HttpPost]
        public ActionResult OrderCreate(string Firstname, string Lastname, string TelephoneNumber, string DeliveryType, string Adress, string Department, string PayType, string Promocode, decimal Price)
        {
            
            
            ApplicationUser user = new ApplicationUser();
            Order order;
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                orderItems = Session["Trash"] as List<OrderItem>;
                
                foreach (var item in orderItems)
                {
                    ctx.Products.Attach(item.Product);
                    ctx.OrderItems.Attach(item);
                }
                products = ctx.Products.ToList();
                Promocode promocode;
                if (Promocode == "")
                {
                    promocode = null;
                }
                else
                {
                    promocode = ctx.Promocodes.FirstOrDefault(p => p.Code == Promocode);
                }
                user = ctx.Users.FirstOrDefault(u => u.UserName == User.Identity.Name);
                
                var accountInfo = new AccountInfo { User = user, Firstname = Firstname, Lastname = Lastname, Adress = Adress, Email = user.Email, TelephoneNumber = TelephoneNumber };
                Random generator = new Random();
                int r;
                do
                {
                    r = generator.Next(100000, 1000000);

                } while (ctx.Orders.Any(o => o.OrderId.Equals(r.ToString())));
                string order_id = r.ToString();
                List<OrderItem> os = ctx.OrderItems.ToList();
                ;
                order = new Order { DateCreated = DateTime.Now, Adress = Adress, DeliveryType = DeliveryType, Department = Department, PayType = PayType, Status = "Ожидает отправки", OrderItems = orderItems, Price = Price, User = user, AccountInfo = accountInfo, Promocode = promocode, OrderId = order_id ,Guid = Guid.NewGuid()};
                ctx.Orders.Add(order);
                
                ctx.SaveChanges();
                List<OrderItem> oss = ctx.OrderItems.ToList();
                ;
            }
            Session.Abandon();
            List<string> vs = new List<string>();
            
            using (StreamReader fs = new StreamReader(Server.MapPath("~/AdminPass/email.txt")))
            {
                while (true)
                {
                    // Читаем строку из файла во временную переменную.
                    string temp = fs.ReadLine();

                    // Если достигнут конец файла, прерываем считывание.
                    if (temp == null) break;

                    // Пишем считанную строку в итоговую переменную.
                    vs.Add(temp);
                }
            }
            string email = vs[0];
            string pass = vs[1];
            MailAddress from = new MailAddress(email, "4protection");
            MailAddress toUser = new MailAddress(order.AccountInfo.Email);
            MailMessage msgToUser = new MailMessage(from, toUser);

            string pathHtml = Server.MapPath("~/AdminPass");
            string templateUser = System.IO.File.ReadAllText(System.IO.Path.Combine(pathHtml, "order_success_user.html"));
            templateUser = templateUser.Replace("{{Lastname}}", order.AccountInfo.Lastname);
            templateUser = templateUser.Replace("{{Firstname}}", order.AccountInfo.Firstname);
            templateUser = templateUser.Replace("{{OrderID}}", order.OrderId);
            templateUser = templateUser.Replace("{{email}}", order.AccountInfo.Email);
            templateUser = templateUser.Replace("{{telNumber}}", order.AccountInfo.TelephoneNumber);
            if (PayType == "1")
            {
                templateUser = templateUser.Replace("{{PayType}}", "Оплата картой");
            }
            else
            {
                templateUser = templateUser.Replace("{{PayType}}", "Оплата при получении");
            }
            if (DeliveryType == "1")
            {
                templateUser = templateUser.Replace("{{DeliveryType}}", "Самовывоз");
            }
            else
            {
                templateUser = templateUser.Replace("{{DeliveryType}}", "Новая почта");
            }
            templateUser = templateUser.Replace("{{Date}}", DateTime.Now.ToShortDateString());
            templateUser = templateUser.Replace("{{Price}}", order.Price.ToString());
            msgToUser.Subject = "Заказ на 4protection.com.ua";
            msgToUser.Body = templateUser;
            msgToUser.IsBodyHtml = true;
            SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
            client.EnableSsl = true;
            client.Credentials = new NetworkCredential(email, pass);
            client.Send(msgToUser);

            MailAddress toAdmin = new MailAddress(email, "4protection");
            MailMessage msgToAdmin = new MailMessage(from, toAdmin);
            string templateAdmin = System.IO.File.ReadAllText(System.IO.Path.Combine(pathHtml, "order_success_admin.html"));
            templateAdmin = templateAdmin.Replace("{{Lastname}}", order.AccountInfo.Lastname);
            templateAdmin = templateAdmin.Replace("{{Firstname}}", order.AccountInfo.Firstname);
            templateAdmin = templateAdmin.Replace("{{OrderID}}", order.OrderId);
            templateAdmin = templateAdmin.Replace("{{email}}", order.AccountInfo.Email);
            templateAdmin = templateAdmin.Replace("{{telNumber}}", order.AccountInfo.TelephoneNumber);
            if (PayType == "1")
            {
                templateAdmin = templateAdmin.Replace("{{PayType}}", "Оплата картой");
            }
            else
            {
                templateAdmin = templateAdmin.Replace("{{PayType}}", "Оплата при получении");
            }
            if (DeliveryType == "1")
            {
                templateAdmin = templateAdmin.Replace("{{DeliveryType}}", "Самовывоз");
            }
            else
            {
                templateAdmin = templateAdmin.Replace("{{DeliveryType}}", "Новая почта");
            }
            templateAdmin = templateAdmin.Replace("{{Date}}", DateTime.Now.ToShortDateString());
            templateAdmin = templateAdmin.Replace("{{Price}}", order.Price.ToString());
            msgToAdmin.Subject = "Заказ на 4protection.com.ua";
            msgToAdmin.Body = templateAdmin;
            msgToAdmin.IsBodyHtml = true;
            client.Send(msgToAdmin);

            if (PayType == "1")
            {
                return RedirectToAction("LiqpayPOST", new { order_guid = order.Guid });
            }
            else
            {
                return RedirectToAction("OrderDone", "Home");
            }


        }
        [RequireHttps]
        public ActionResult LiqpayPOST(Guid order_guid)
        {
            decimal amount;
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                amount = ctx.Orders.FirstOrDefault(o => o.Guid == order_guid).Price;
            }
            return View(LiqPayHelper.GetLiqPayModel(order_guid.ToString(), amount));
        }
        [RequireHttps]
        [Authorize(Roles = RoleConfig.ADMIN)]
        public ActionResult OrderAdmin()
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                promocodes = ctx.Promocodes.ToList();
                products = ctx.Products.ToList();
                orders = ctx.Orders.ToList();
                orderItems = ctx.OrderItems.ToList();
                users = ctx.Users.ToList();
                accountInfos = ctx.AccountInfos.ToList();
            }
            return View(orders);

        }
        [RequireHttps]
        [Authorize(Roles = RoleConfig.ADMIN)]
        public ActionResult OrderDoneAdmin(int? id)
        {
            if (id is null)
            {
                return RedirectToAction("OrderAdmin");
            }
            else
            {
                using (ApplicationDbContext ctx = new ApplicationDbContext())
                {
                    var order = ctx.Orders.FirstOrDefault(o => o.Id == id);
                    order.Status = "Отправлен";
                    ctx.SaveChanges();
                }
                return RedirectToAction("OrderAdmin");
            }

        }
        [RequireHttps]
        [Authorize(Roles = RoleConfig.ADMIN)]
        public ActionResult EditOrderAdmin(int? id)
        {
            if (id != null)
            {
                var order = new Order();
                using (ApplicationDbContext ctx = new ApplicationDbContext())
                {
                    orderItems = ctx.OrderItems.ToList();
                    users = ctx.Users.ToList();
                    accountInfos = ctx.AccountInfos.ToList();
                    order = ctx.Orders.FirstOrDefault(a => a.Id == id);
                }
                return View(order);
            }
            else
            {
                return RedirectToAction("OrderAdmin");
            }

        }
        [RequireHttps]
        [Authorize(Roles = RoleConfig.ADMIN)]
        [HttpPost]
        public ActionResult EditOrderAdmin(int id, string Firstname, string Lastname, string TelephoneNumber, string DeliveryType, string Adress, string Department)
        {
            if (Firstname != null && Lastname != null && TelephoneNumber != null)
            {
                using (ApplicationDbContext ctx = new ApplicationDbContext())
                {
                    users = ctx.Users.ToList();
                    accountInfos = ctx.AccountInfos.ToList();
                    var Order = ctx.Orders.FirstOrDefault(o => o.Id == id);
                    Order.AccountInfo.Firstname = Firstname;
                    Order.AccountInfo.Lastname = Lastname;
                    Order.AccountInfo.TelephoneNumber = TelephoneNumber;
                    if (DeliveryType != null)
                    {
                        Order.Adress = Adress;
                        Order.Department = Department;
                    }

                    ctx.SaveChanges();
                }
                return RedirectToAction("OrderAdmin");
            }
            else
            {
                return RedirectToAction("OrderAdmin");
            }

        }
        public ActionResult SearchByTelephoneNumber(string name)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                promocodes = ctx.Promocodes.ToList();
                products = ctx.Products.ToList();
                orderItems = ctx.OrderItems.ToList();
                users = ctx.Users.ToList();
                accountInfos = ctx.AccountInfos.ToList();
                var res = ctx.Orders.Where(o => o.AccountInfo.TelephoneNumber.Contains(name)).ToList();

                return PartialView("_ViewOrders", res);
            }
        }
        public ActionResult SearchByOrderId(string name)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                promocodes = ctx.Promocodes.ToList();
                products = ctx.Products.ToList();
                orderItems = ctx.OrderItems.ToList();
                users = ctx.Users.ToList();
                accountInfos = ctx.AccountInfos.ToList();
                var res = ctx.Orders.Where(o => o.OrderId.Contains(name)).ToList();

                return PartialView("_ViewOrders", res);
            }
        }

        [RequireHttps]
        public ActionResult OrderDone()
        {
            return View();
        }
        [RequireHttps]
        public ActionResult FAQ()
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                faqs = ctx.FAQs.ToList();
            }
            return View(faqs);
        }
        [RequireHttps]
        [Authorize(Roles = RoleConfig.ADMIN)]
        public ActionResult EditFAQ(int? id)
        {
            if (id != null)
            {
                var faq = new FAQ();
                using (ApplicationDbContext ctx = new ApplicationDbContext())
                {
                    faq = ctx.FAQs.FirstOrDefault(f => f.Id == id);
                }
                return View(faq);
            }
            else
            {
                return RedirectToAction("FAQ");
            }

        }
        [RequireHttps]
        [Authorize(Roles = RoleConfig.ADMIN)]
        [HttpPost]
        public ActionResult EditFAQ(FAQ faq)
        {
            if (faq != null)
            {
                using (ApplicationDbContext ctx = new ApplicationDbContext())
                {
                    var Faq = ctx.FAQs.FirstOrDefault(f => f.Id == faq.Id);
                    Faq.Answer = faq.Answer;
                    Faq.Question = faq.Question;
                    ctx.SaveChanges();
                }
                return RedirectToAction("FAQ");
            }
            else
            {
                return RedirectToAction("FAQ");
            }

        }
        [RequireHttps]
        [Authorize(Roles = RoleConfig.ADMIN)]
        public ActionResult CreateFAQ()
        {
            return View();
        }
        [RequireHttps]
        [Authorize(Roles = RoleConfig.ADMIN)]
        [HttpPost]
        public ActionResult CreateFAQ(FAQ faq)
        {
            if (faq != null)
            {
                using (ApplicationDbContext ctx = new ApplicationDbContext())
                {

                    FAQ Faq = new FAQ { Answer = faq.Answer, Question = faq.Question };
                    ctx.FAQs.Add(Faq);
                    ctx.SaveChanges();
                }
                return RedirectToAction("FAQ");
            }
            else
            {
                return RedirectToAction("FAQ");
            }

        }
        [RequireHttps]
        [Authorize(Roles = RoleConfig.ADMIN)]
        public ActionResult DeleteFAQ(int? id)
        {
            if (id != null)
            {
                using (ApplicationDbContext ctx = new ApplicationDbContext())
                {
                    ctx.FAQs.Remove(ctx.FAQs.FirstOrDefault(f => f.Id == id));
                    ctx.SaveChanges();
                }
                return RedirectToAction("FAQ");
            }
            else
            {
                return RedirectToAction("FAQ");
            }

        }
        [RequireHttps]
        [Authorize(Roles = RoleConfig.ADMIN)]
        public ActionResult AddSlider()
        {
            return View();
        }
        [RequireHttps]
        [Authorize(Roles = RoleConfig.ADMIN)]
        [HttpPost]
        public ActionResult AddSlider(HttpPostedFileBase uploadFile, string Url)
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {

                if (uploadFile != null && uploadFile.ContentLength > 0)
                {
                    SliderImage slider = new SliderImage { Path = $"Images/{uploadFile.FileName}", Url= Url };
                    string filePath = Path.Combine(Server.MapPath("/Images"), Path.GetFileName(uploadFile.FileName));
                    uploadFile.SaveAs(filePath);
                    ctx.SliderImages.Add(slider);
                    ctx.SaveChanges();
                }

            }
            return RedirectToAction("Index");

        }
        [RequireHttps]
        [Authorize(Roles = RoleConfig.ADMIN)]
        public ActionResult DeleteSlider(int? id)
        {
            if (id != null)
            {
                using (ApplicationDbContext ctx = new ApplicationDbContext())
                {
                    ctx.SliderImages.Remove(ctx.SliderImages.FirstOrDefault(p => p.Id == id));
                    ctx.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }
        [RequireHttps]
        public ActionResult License()
        {

            return View();
        }
        [RequireHttps]
        public ActionResult LicenseDelivery()
        {

            return View();
        }
    }


}