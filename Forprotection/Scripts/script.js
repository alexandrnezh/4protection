$(document).ready(function () {
    let counter = sessionStorage.getItem('storeCounter');
    if (counter == null) {
        sessionStorage.setItem('storeCounter', 0);
        counter = sessionStorage.getItem('storeCounter');
    }

    $('.container_count').text(counter);

    $('.addProduct').each(function () {
        $(this).click(function () {
            counter++;
            sessionStorage.setItem('storeCounter', counter);
        });
    });

    $('.OrderCreate').on('click', function () {
        counter = 0;
        sessionStorage.setItem('storeCounter', counter);
    });

    $('.deleteProduct').each(function () {
        $(this).click(function () {
            counter--;
            sessionStorage.setItem('storeCounter', counter);

        });
    });

    $('.mobile_menu').on('click', function (e) {
        e.preventDefault();
        $('.menu-btn').toggleClass('menu_active')
        $('.menu_wrap').toggleClass('menu_active')
    });
    $('.article-image').each(function () {
        $(this).click(function () {
            if ($(this).parent().find('.article-text').css('display') == 'none') {

                $(this).parent().find('.article-text').css('display', 'block');
                var height = parseInt($(this).parent().find('.article-text').css('height').replace('px', '')) + parseInt($(this).parent().css('height').replace('px', '')) + 100;
                $(this).parent().css('height', height);
                $(this).parent().find('.article-text').css('display', 'none');
                $(this).parent().find('.article-text').fadeIn();
            } else {
                if (window.matchMedia("(min-width: 1201px)").matches) {
                    $(this).parent().find('.article-text').css('display', 'none');
                    $(this).parent().css('height', 400);
                }
                if (window.matchMedia("(max-width: 1201px)").matches) {
                    $(this).parent().find('.article-text').css('display', 'none');
                    $(this).parent().css('height', 300);
                }
                if (window.matchMedia("(max-width: 911px)").matches) {
                    $(this).parent().find('.article-text').css('display', 'none');
                    $(this).parent().css('height', 200);
                }
                if (window.matchMedia("(max-width: 551px)").matches) {
                    $(this).parent().find('.article-text').css('display', 'none');
                    $(this).parent().css('height', 127);
                }
            }
        });
    });
    $('.owl-carousel').owlCarousel({
        loop: true,
        autoplay: true,
        autoplayTimeout: 5000,
        nav: false,
        responsive: {
            0: {
                items: 1

            }
        }
    });
    var coll = document.getElementsByClassName("collapsible");
    var i;

    for (i = 0; i < coll.length; i++) {
        coll[i].addEventListener("click", function (e) {

            if (this.classList.contains('login')) {
                e.preventDefault();
            }
            this.classList.toggle("active");
            var content = this.nextElementSibling;
            if (content.style.display === "block") {
                content.style.display = "none";
            } else {
                content.style.display = "block";
            }
        });
    }

});