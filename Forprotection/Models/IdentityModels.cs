﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Forprotection.Models.DB;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Forprotection.Models
{
    // В профиль пользователя можно добавить дополнительные данные, если указать больше свойств для класса ApplicationUser. Подробности см. на странице https://go.microsoft.com/fwlink/?LinkID=317594.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Обратите внимание, что authenticationType должен совпадать с типом, определенным в CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Здесь добавьте утверждения пользователя
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public DbSet<AccountInfo> AccountInfos { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<ArticleImage> ArticleImages { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductImage> ProductImages { get; set; }
        public DbSet<Promocode> Promocodes { get; set; }
        public DbSet<FAQ> FAQs { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<SliderImage> SliderImages { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }

    public class DatabaseInitializer : CreateDatabaseIfNotExists<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext ctx)
        {
            var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(ctx));

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(ctx));

            ProductImage pi1 = new ProductImage { Path = "Images/1.png" };
            ProductImage pi2 = new ProductImage { Path = "Images/2.png" };
            ProductImage pi3 = new ProductImage { Path = "Images/3.png" };
            ProductImage pi4 = new ProductImage { Path = "Images/4.png" };
            ProductImage pi5 = new ProductImage { Path = "Images/5.png" };
            ProductImage pi6 = new ProductImage { Path = "Images/6.png" };
            ProductImage pi7 = new ProductImage { Path = "Images/7.png" };
            ProductImage pi8 = new ProductImage { Path = "Images/8.png" };
            ProductImage pi9 = new ProductImage { Path = "Images/1.png" };
            Product p1 = new Product { Name = "Протеин", Description = "Структурный протеин на основе  легкоусвояемого молочного белка.  Не содержит лактозы. Имеет все  важные витамины и микроэлементы  для регенерации и построения  мышечной ткани.", ExtraDescription= "Применять через 20-30мин после  тренировки", Image = pi1, Price = (decimal)62.90, DateCreated = DateTime.Now , Nal = "В наличии"};
            Product p2 = new Product { Name = "Basocaps", Description = "Кислотный бустер. Сводит к минимуму  вредные окислительные процессы во  время тренировки, нейтрализирует  молочную, мочевую кислоту и пр.", ExtraDescription = "Принимать за 1-2 часа до тренировки", Image = pi2, Price = (decimal)109, DateCreated = DateTime.Now, Nal = "В наличии" };
            Product p3 = new Product { Name = "Acticaps", Description = "Комплекс микроэлементов  (хондропротектор). Снимает воспаление  с суставов и связок, способствует  регенерации. Стимулирует выработку  коллагена.", ExtraDescription = "Принимать по 3 табл в день  (независимо от тренировок)", Image = pi3, Price = (decimal)139, DateCreated = DateTime.Now, Nal = "В наличии" };
            Product p4 = new Product { Name = "Recovery drink", Description = "Способствует быстрому восстановлению  организма после тренировки.  Содержит незаменимые кислоты (BCAA)  и витамины.  Базой является минеральная вода.", ExtraDescription = "Принимать сразу после тренировки", Image = pi4, Price = (decimal)3.90, DateCreated = DateTime.Now, Nal = "В наличии" };
            Product p5 = new Product { Name = "Starter", Description = "Предтренировочная добавка. Улучшает  концентрацию, выносливость,  способствует жиросжиганию, снижает  нервозность.", ExtraDescription = "Принимать за 10-20 мин до тренировки ", Image = pi5, Price = (decimal)48, DateCreated = DateTime.Now, Nal = "В наличии" };
            Product p6 = new Product { Name = "Permanent+", Description = "Комплекс витаминов и минералов.  Нормализует баланс минералов,  которые активно теряются во время  тренировки. Повышает выносливость. ", ExtraDescription = "Принимать во время тренировки", Image = pi6, Price = (decimal)48, DateCreated = DateTime.Now, Nal = "В наличии" };
            Product p7 = new Product { Name = "Tablets OM 24", Description = "Мощный комплекс витаминов,  минералов, микроэлементов и  аминокислот.  Повышает умственную активность и  работоспособность всего организма.  Нормализует обмен веществ.  Положительно влияет на нервную  систему, снижает нервозность и  повышает концентрацию.  Снижает частоту сердечных  сокращений, способствует  восстановлению.", ExtraDescription = "Принимать по 1-2 табл в день  (независимо от тренировки)", Image = pi7, Price = (decimal)159, DateCreated = DateTime.Now, Nal = "В наличии" };
            Product p8 = new Product { Name = "Sport DOUCHE", Description = "Гель для душа. Успокаивает кожу после  нагрузок, нейтрализует окислительные  процессы в коже, вызывающие  старение.", ExtraDescription = "Применять во время душа после  тренировки.", Image = pi8, Price = (decimal)30, DateCreated = DateTime.Now, Nal = "В наличии" };
            Product p9 = new Product { Name = "Actigel", Description = "Гель для внешнего применения.  Снимает воспаление с мышц и связок  после тренировки. Эффективно  снижает крепатуру.", ExtraDescription = "Наносить на чистое тело срразу после  тренировки. При необходимости  повторить нанесение.", Image = pi9, Price = (decimal)85, DateCreated = DateTime.Now, Nal = "В наличии" };

            var adminRole = new IdentityRole(RoleConfig.ADMIN);
            var userRole = new IdentityRole(RoleConfig.USER);

            roleManager.Create(adminRole);
            roleManager.Create(userRole);


            var admin = new ApplicationUser { UserName = "dmytrogolubev@gmail.com", Email = "dmytrogolubev@gmail.com" };

            userManager.Create(admin, "admin1");

            userManager.AddToRole(admin.Id, RoleConfig.ADMIN);
            userManager.AddToRole(admin.Id, RoleConfig.USER);

            ArticleImage ai1 = new ArticleImage { Path = "Images/image1.jpg" };
            Article a1 = new Article {Name = "Article1", Body = "Body1", Image = ai1, DateCreated = DateTime.Now, User = admin };


            Promocode promocode1 = new Promocode { Code = "promo1", DiscountPercent = 15, User = admin };
            Promocode promocode2 = new Promocode { Code = "promo2", DiscountPercent = 25, User = admin };
            Promocode promocode3 = new Promocode { Code = "promo3", DiscountPercent = 15, User = admin };

            SliderImage sliderImage1 = new SliderImage { Path = "Images/slider-1.jpg" };
            SliderImage sliderImage2 = new SliderImage { Path = "Images/slider-2.jpg" };
            SliderImage sliderImage3 = new SliderImage { Path = "Images/slider-3.jpg" };
            SliderImage sliderImage4 = new SliderImage { Path = "Images/slider-4.jpg" };

            FAQ faq1 = new FAQ { Question = "Lorem ipsum dolor sit amet, consectetur adipisicing elit?", Answer = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus ut eligendi amet vero dicta sequi saepe voluptatibus corporis quisquam, quo!" };
            FAQ faq2 = new FAQ { Question = "Lorem ipsum dolor sit amet, consectetur adipisicing elit?", Answer = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus ut eligendi amet vero dicta sequi saepe voluptatibus corporis quisquam, quo!" };
            FAQ faq3 = new FAQ { Question = "Lorem ipsum dolor sit amet, consectetur adipisicing elit?", Answer = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus ut eligendi amet vero dicta sequi saepe voluptatibus corporis quisquam, quo!" };

            ctx.Products.AddRange(new List<Product>() { p1, p2, p3, p4, p5, p6,p7,p8,p9 });
            ctx.Articles.Add(a1);
            ctx.Promocodes.AddRange(new List<Promocode>() { promocode1,promocode2,promocode3 });
            ctx.FAQs.AddRange(new List<FAQ>() { faq1, faq2, faq3 });
            ctx.SliderImages.AddRange(new List<SliderImage>() { sliderImage1, sliderImage2, sliderImage3, sliderImage4 });
            ctx.SaveChanges();
        }
    }
}