﻿using Forprotection.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Forprotection.Models
{
    public class PromocodeViewModel
    {
        public Promocode Promocode { get; set; }
        public decimal Price { get; set; }
    }
}