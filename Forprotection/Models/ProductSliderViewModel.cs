﻿using Forprotection.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Forprotection.Models
{
    public class ProductSliderViewModel
    {
        public List<Product> Products { get; set; }
        public List<SliderImage> SliderImages { get; set; }
    }
}