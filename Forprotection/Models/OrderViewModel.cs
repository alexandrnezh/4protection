﻿using Forprotection.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Forprotection.Models
{
    public class OrderViewModel
    {
        public List<Order> Orders { get; set; }
        public List<AccountInfo> Accounts { get; set; }
    }
}