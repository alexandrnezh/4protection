﻿using Forprotection.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Forprotection.Models
{
    public class ProductViewModel
    {
        public Product Product { get; set; }
        public List<Comment> Comments { get; set; }
    }
}