﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Forprotection.Models.DB
{
    public class Order
    {
        public int Id { get; set; }
        public string Status { get; set; }
        public string DeliveryType { get; set; }
        public string Adress { get; set; }
        public string Department { get; set; }
        public Promocode Promocode { get; set; }
        public string PayType { get; set; }
        public decimal Price { get; set; }
        public DateTime DateCreated { get; set; }
        public ApplicationUser User { get; set; }
        public AccountInfo AccountInfo { get; set; }
        public List<OrderItem> OrderItems { get; set; }
        public string OrderId { get; set; }
        public Guid Guid { get; set; }
    }
}