﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Forprotection.Models.DB
{
    public class Article
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Body { get; set; }
        public DateTime DateCreated { get; set; }
        public ArticleImage Image { get; set; }
        public ApplicationUser User { get; set; }
    }
}