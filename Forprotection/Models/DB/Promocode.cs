﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Forprotection.Models.DB
{
    public class Promocode
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int DiscountPercent { get; set; }
        public ApplicationUser User { get; set; }
    }
}