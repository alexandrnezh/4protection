﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Forprotection.Models.DB
{
    public class ProductImage
    {
        public int Id { get; set; }
        public string Path { get; set; }
    }
}