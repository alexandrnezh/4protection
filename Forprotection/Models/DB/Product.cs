﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Forprotection.Models.DB
{
    public class Product
    {
        public int Id { get; set; }
        public ProductImage Image { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public string ExtraDescription { get; set; }
        public string Nal { get; set; }
        public DateTime DateCreated { get; set; }
    }
}