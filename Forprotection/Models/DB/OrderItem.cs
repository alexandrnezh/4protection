﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Forprotection.Models.DB
{
    public class OrderItem
    {
        public int Id { get; set; }
        public Guid Guid { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public Product Product { get; set; }
    }
}