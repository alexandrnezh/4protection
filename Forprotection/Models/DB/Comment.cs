﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Forprotection.Models.DB
{
    public class Comment
    {
        public int Id { get; set; }
        public string Nickname { get; set; }
        public string Text { get; set; }
        public Product Product { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}