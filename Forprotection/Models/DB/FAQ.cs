﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Forprotection.Models.DB
{
    public class FAQ
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}